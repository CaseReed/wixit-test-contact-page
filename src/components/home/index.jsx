import React from 'react';

const Home = () => (
  <article className="shadow overflow-hidden sm:rounded-md px-4 py-5 bg-white sm:p-6">
    <h1 className="text-lg font-medium leading-6 text-blue-900">Test technique</h1>
    <h2 className="mt-5 text-md font-medium leading-6 text-gray-900">Description</h2>
    <p className="mt-1 text-sm text-gray-600">Pour cet exercice, il est demandé de réaliser les tâches suivantes :</p>
    <ol className="mt-1 list-decimal list-inside text-sm text-gray-600">
      <li className="mt-1">
        Créer une page menant à l‘URL `/contact` qui utilisera le composant `Contact`.
      </li>
      <li className="mt-1">
        Rendre dynamique le formulaire avec les librairies `Formik` et `Yup`, et valider les champs avec les règles suivantes :
        <ul className="ml-5 mt-1 list-disc list-inside">
          <li>Le champ `Prénom` est requis;</li>
          <li>Le champ `Nom` est requis;</li>
          <li>
            Le champ `Email` :
            <ul className="ml-5 mt-1 list-disc list-inside">
              <li>est requis;</li>
              <li>doit être un email valide;</li>
            </ul>
          </li>
          <li>Le champ `Type` est requis;</li>
          <li>
            Le champ `Numéro de commande` :
            <ul className="ml-5 mt-1 list-disc list-inside">
              <li>doit avoir pour format : 2 lettres majuscules, un tiret, et 5 chiffres (ex. AB-12345)</li>
              <li>est masqué seulement si le champ `Type` a pour valeur 'Contact';</li>
              <li>est requis seulement si le champ `Type` a pour valeur 'Réclamation';</li>
            </ul>
          </li>
          <li>Le champ `Objet` est requis;</li>
          <li>
            Le champ `Message` :
            <ul className="ml-5 mt-1 list-disc list-inside">
              <li>est requis;</li>
              <li>ne doit comporter que 500 caractères maximum;</li>
            </ul>
          </li>
        </ul>
      </li>
      <li className="mt-1">
        Afficher les messages d'erreur sous les champs du formulaire, en ayant le style `.form-error`.
      </li>
      <li className="mt-1">
        A la validation du formulaire, retourner un `console.log` comportant les valeurs des différents champs.
      </li>
      <li className="mt-1">
        Rendre responsive le style du composant `Contact`. En-dessous de <strong>640px</strong>, le contenu passe sur 1 seule colonne.
      </li>
    </ol>
    <h2 className="mt-5 text-md font-medium leading-6 text-gray-900">Ressources</h2>
    <ul className="mt-1 list-disc list-inside text-sm text-gray-600">
      <li><a className="text-blue-600" href="https://reactrouter.com/web/api/">https://reactrouter.com/web/api/</a></li>
      <li><a className="text-blue-600" href="https://tailwindcss.com">https://tailwindcss.com</a></li>
      <li><a className="text-blue-600" href="https://formik.org">https://formik.org</a></li>
      <li><a className="text-blue-600" href="https://github.com/jquense/yup">https://github.com/jquense/yup</a></li>
    </ul>
  </article>
);

export default Home;
