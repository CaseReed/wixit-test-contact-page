import React from 'react';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';

const Contact = () => {
  const validate = Yup.object().shape({
    firstName: Yup.string()
      .min(2, "Trop court !")
      .max(25, "Trop long !")
      .required("Le prénom est requis"),
    lastName: Yup.string()
      .min(2, "Trop court !")
      .max(25, "Trop long !")
      .required("Le nom est requis"),
    email: Yup.string()
      .email('Email invalide')
      .required('Email requis'),
    type: Yup.string()
      .required('Le type est requis'),
    orderNumber: Yup.string()
      .required('Le numéro de commande est requis')
      .matches(/([A-Z]){2}([-]){1}([0-9]){5}/, "Le format n'est pas correct"),
    subject: Yup.string()
      .min(5, "Trop court !")
      .max(50, "Trop long !")
      .required('Object requis'),
    message: Yup.string()
      .min(5, "Trop court !")
      .max(500, "Trop long !")
      .required('Message requis'),
  })

  return (
    <div className="grid grid-cols-1 gap-6 sm:grid-cols-3">
      <div className="col-span-1">
        <div className="px-4 sm:px-0">
          <h3 className="text-lg font-medium leading-6 text-gray-900">Contact</h3>
          <p className="mt-1 text-sm text-gray-600">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Pariatur dolor est animi ex architecto ad et adipisci officia error doloremque cupiditate nam voluptates laboriosam aliquam sunt sapiente, quod temporibus officiis.
          </p>
        </div>
      </div>
      <div className="col-span-2">
        <Formik
          initialValues={{
            firstName: '',
            lastName: '',
            email: '',
            type: '',
            orderNumber: '',
            subject: '',
            message: ''
          }}
          onSubmit={initialValues => {
            console.log(initialValues);
          }}
          validationSchema={validate}
        >
          {({ values, handleChange, handleBlur, handleSubmit, errors, touched }) => (
            <form onSubmit={handleSubmit}>
              <div className="shadow overflow-hidden sm:rounded-md">
                <div className="px-4 py-5 bg-white sm:p-6">
                  <div className="grid grid-cols-6 gap-6">
                    <div className="col-span-3">
                      <label htmlFor="firstName" className="form-label">
                        Prénom
                      </label>
                      <Field name="firstName" type="input" className="form-input" />
                      {errors.firstName && touched.firstName ? (
                        <div className="form-error">{errors.firstName}</div>
                      ) : null}
                    </div>

                    <div className="col-span-3">
                      <label htmlFor="lastName" className="form-label">
                        Nom
                      </label>
                      <Field name="lastName" type="input" className="form-input" />
                      {errors.lastName && touched.lastName ? (
                        <div className="form-error">{errors.lastName}</div>
                      ) : null}
                    </div>

                    <div className="col-span-4">
                      <label htmlFor="email" className="form-label">
                        Email
                      </label>
                      <Field name="email" type="input" className="form-input" />
                      {errors.email && touched.email ? (
                        <div className="form-error">{errors.email}</div>
                      ) : null}
                    </div>

                    <div className="col-span-3">
                      <label htmlFor="type" className="form-label">
                        Type
                      </label>
                      <Field name="type" as="select" type="select" className="form-select">
                        <option value="" disabled>Sélectionner</option>
                        <option value="contact">Contact</option>
                        <option value="claim">Réclamation</option>
                      </Field>
                      {errors.type && touched.type ? (
                        <div className="form-error">{errors.type}</div>
                      ) : null}
                    </div>

                    <div className="col-span-3">
                      {values.type === "claim" ? (
                        <>
                          <label htmlFor="orderNumber" className="form-label">Numéro de commande</label>
                          <Field name="orderNumber" type="input" className="form-input" />
                          <div className="form-error">{errors.orderNumber}</div>
                        </>
                      ) : null}
                    </div>

                    <div className="col-span-4">
                      <label htmlFor="subject" className="form-label">
                        Objet
                      </label>
                      <Field name="subject" type="input" className="form-input" />
                      {errors.subject && touched.subject ? (
                        <div className="form-error">{errors.subject}</div>
                      ) : null}
                    </div>

                    <div className="col-span-6">
                      <label htmlFor="message" className="form-label">
                        Message
                      </label>
                      <Field name="message" as="textarea" rows="5" className="form-input" />
                      {errors.message && touched.message ? (
                        <div className="form-error">{errors.message}</div>
                      ) : null}
                    </div>
                  </div>
                </div>
                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                  <button type="submit" className="btn">
                    Envoyer
                  </button>
                </div>
              </div>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default Contact;
