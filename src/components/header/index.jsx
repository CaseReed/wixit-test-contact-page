import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => (
  <nav className="bg-white shadow">
    <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
      <div className="flex justify-between h-16">
        <div className="flex space-x-8">
          <NavLink
            to="/"
            exact
            activeClassName="border-indigo-500"
            className="border-transparent text-gray-900 inline-flex items-center px-1 pt-1 border-b-2 text-sm font-medium">
            Accueil
          </NavLink>
          <NavLink
            to="/contact"
            activeClassName="border-indigo-500"
            className="border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700 inline-flex items-center px-1 pt-1 border-b-2 text-sm font-medium">
            Contact
          </NavLink>
        </div>
      </div>
    </div>
  </nav>
);

export default Header;
