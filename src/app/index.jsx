import React from 'react';
import { Switch, Router, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import Home from '../components/home';
import Contact from '../components/contact'
import Layout from '../components/layout';

const App = () => {
  const history = createBrowserHistory();

  return (
    <Router history={history}>
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/contact" component={Contact} />
        </Switch>
      </Layout>
    </Router>
  );
};

export default App;
